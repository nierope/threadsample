﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
    
namespace ThreadSample
{
    public delegate void ConversionTaskCompleted(string taskId, bool isError);
    public delegate void ConversionTaskWorking(string taskId, int i);

    internal class ConversionThread
    {

        private string _taskId;
        private ConversionTaskCompleted _completedCallback;
        private ConversionTaskWorking _workingCallback;

        internal string TaskId
        {
            get { return _taskId; }
            set { _taskId = value; }
        }

        internal ConversionTaskCompleted CompletedCallback
        {
            get { return _completedCallback; }
            set { _completedCallback = value; }
        }

        internal ConversionTaskWorking WorkingCallback
        {
            get { return _workingCallback; }
            set { _workingCallback = value; }
        }

        internal void ExecuteThreadTask()
        {

            bool isError = false;

            for (int i = 0; i < 10; i++)
            {
                _workingCallback.Invoke(_taskId,i);
                Thread.Sleep(2000);
            }
            _completedCallback.Invoke(_taskId, isError);
        }

    }
}
