﻿namespace ThreadSample
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;

    public delegate void WorkerStarted(string taskid);
    public delegate void WorkerCompleted(string taskid);
    public delegate void WorkerProgressChanged(string taskid, int percent);

    public class ConversionWorker
    {

        private Thread p_thread;

        private WorkerCompleted _completed;
        private WorkerStarted _started;
        private WorkerProgressChanged _progresschanged;

        /// <summary>
        /// Stop the thread
        /// </summary>
        public void Stop()
        {
            if(p_thread.IsAlive) 
                p_thread.Abort(); 
        }

        internal WorkerStarted Started
        {
            get { return _started; }
            set { _started = value; }
        }

        internal WorkerCompleted Completed
        {
            get { return _completed; }
            set { _completed = value; }
        }

        internal WorkerProgressChanged ProgressChanged
        {
            get { return _progresschanged; }
            set { _progresschanged = value; }
        }

        /// <summary>
        /// Work
        /// </summary>
        public void Work()
        {
            try
            {
                ConversionThread threadTask = new ConversionThread();

                threadTask.TaskId = "MyTask" + DateTime.Now.Ticks.ToString();

                threadTask.CompletedCallback =
                    new ConversionTaskCompleted(ConversionTaskCompletedCallback);

                threadTask.WorkingCallback =
                    new ConversionTaskWorking(ConversionTaskWorkingCallback);

                p_thread = new Thread(threadTask.ExecuteThreadTask);
                p_thread.Start();

                _started.Invoke(threadTask.TaskId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Method that receives callbacks from threads upon completion.
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="isError"></param>
        public void ConversionTaskCompletedCallback(string taskId, bool isError)
        {


            _completed.Invoke(taskId);
            
        }

        /// <summary>
        /// Method that receives callbacks from threads while working.
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="isError"></param>
        public void ConversionTaskWorkingCallback(string taskId,int i)
        {
            _progresschanged.Invoke(taskId,i);  
            
        }
    }

}