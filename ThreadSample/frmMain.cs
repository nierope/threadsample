﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace ThreadSample
{
    delegate void SetTextCallback(string text);
    delegate void SetProgressCallback(int value);

    /// <summary>
    /// 
    /// Class frmMain
    /// </summary>
    public partial class frmMain : Form
    {

        ConversionWorker x = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
        }

        //=============================================================================================================================
        // Threading using a... thread!
        //=============================================================================================================================

        /// <summary>
        /// Eventhandler for button1 click event
        /// </summary>
        /// <param name="sender">Button object</param>
        /// <param name="e">Event parameters</param>
        private void button1_Click(object sender, EventArgs e)
        {

            x = new ConversionWorker();

            // set callbacks
            x.Completed += new WorkerCompleted(cbCompleted);
            x.Started += new WorkerStarted(cbStarted);
            x.ProgressChanged += new WorkerProgressChanged(cbProgress);

            

            // start working
            x.Work();
       

            // and proceed
            for (int intCount = 1; intCount < 30; intCount++)
            {
                //Thread.Sleep(1000);
                lstOutput.Items.Add("I continue whatever I was planning to do here (" + intCount + ")");
                Application.DoEvents();
            }

            
        }

        /// <summary>
        /// Callback for completed event
        /// </summary>
        /// <param name="taskid">Task id of calling worker thread</param>
        private void cbCompleted(string taskid)
        {
            OutputMessage("(" + taskid  + ") Completed");
        }

        /// <summary>
        /// Callback for started event
        /// </summary>
        /// <param name="taskid">Task id of calling worker thread</param>
        private void cbStarted(string taskid)
        {
            pgbProgress.Value = 0;
            OutputMessage("(" + taskid + ") Started");
        }

        /// <summary>
        /// Callback for progress event
        /// </summary>
        /// <param name="taskid">Task id of calling worker thread</param>
        private void cbProgress(string taskid,int i)
        {
            OutputMessage("(" + taskid + ") Ready for " + (i+1) + " percent");
            //It's on a different thread, so use Invoke.
            SetProgressCallback d = new SetProgressCallback(SetProgress);
            this.Invoke(d, new object[] { i+1 });
        }

        private void OutputMessage(string text)
        {
            //if(this.lstOutput.InvokeRequired)
            
            //It's on a different thread, so use Invoke.
            SetTextCallback d = new SetTextCallback(SetText);
            this.Invoke(d, new object[] { text + " (Invoke)" });
        }
        
        /// <summary>
        /// Sets tekst in listbox
        /// </summary>
        /// <param name="text">Tekst to add to listbox</param>
        private void SetText(string text)
        {
            this.lstOutput.Items.Add(text);
        }
        
        /// <summary>
        /// Sets tekst in listbox
        /// </summary>
        /// <param name="text">Tekst to add to listbox</param>
        private void SetProgress(int value)
        {
            pgbProgress.Value = value;
        }



        //=============================================================================================================================
        // Threading using a backgroundworker
        //=============================================================================================================================

        /// <summary>
        /// Event handler for button2 click event
        /// </summary>
        /// <param name="sender">Button object</param>
        /// <param name="e">Event parameters</param>
        private void button2_Click(object sender, EventArgs e)
        {
            // only start work when not busy
            if (!this.backgroundWorker1.IsBusy)
            {
                this.backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
                this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
                this.backgroundWorker1.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Event handler for backgroundWorker DoWork event 
        /// </summary>
        /// <param name="sender">BackgroundWorker object</param>
        /// <param name="e">Event parameters</param>
         void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
             // Do something 
            Thread.Sleep(2000);
        }

         /// <summary>
         /// Event handler for backgroundWorker runworkercompleted event 
         /// </summary>
         /// <param name="sender">BackgroundWorker object</param>
         /// <param name="e">Event parameters</param>
        private void backgroundWorker1_RunWorkerCompleted(object sender,RunWorkerCompletedEventArgs e)
        {
            // tell the world it is finished working
            lstOutput.Items.Add("Written by the main thread after the background thread completed.");
        }

        /// <summary>
        /// Eventhandler for button3 click event
        /// </summary>
        /// <param name="sender">Button object</param>
        /// <param name="e">Event parameters</param>
        private void button3_Click(object sender, EventArgs e)
        {
            // stop the thread
            lstOutput.Items.Add("User stopped the proces");
            if(x != null)
                x.Stop();
        }

        /// <summary>
        /// Event handler for frmMain form closing event
        /// </summary>
        /// <param name="sender">Form object</param>
        /// <param name="e">Event parameters</param>
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            // stop the thread
            lstOutput.Items.Add("Program stopped the proces");
            if (x != null)
                x.Stop();
        }
    }
}
